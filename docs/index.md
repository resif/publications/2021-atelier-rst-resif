# Utilisation des services Epos-France Sismologie

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Licence Creative Commons Attribution 4.0 International</a>.

## Introduction

Objectif de l'atelier : savoir comment identifier et accéder aux données sismologiques de Résif/Epos-France


Déroulé de l'atelier :

* récupérer les informations du séisme magnitude 5.3, proche de Niort (webservice event) ;
* récupérer la liste des stations actives dans un certain rayon autour du séisme (webservice station) ;
* vérifier la disponibilité des données de ces stations (webservice availability) ;
* récupérer les données du séisme (webservice dataselect) ;
* analyser les données ;
* citer les données ;
* utiliser les portails.


## Contact

Pour toute question, remarque ou besoin de plus d’information, une aide à l’utilisateur est disponible via [https://gitlab.com/resif/sismo-help](https://gitlab.com/resif/sismo-help)
ou en envoyant un message à sismo-help@resif.fr.
