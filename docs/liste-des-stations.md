#  Obtenir la liste des stations

Nous allons rechercher toutes les stations qui enregistraient dans un certain rayon autour de l'épicentre.

Pour cela, on utilise le webservice `fdsn-station`.

Résif-DC propose un URL builder pour vous aider à formuler la requête : [https://seismology.resif.fr/station-url-builder/](https://seismology.resif.fr/station-url-builder/#/)


Choisir la zone géographique concernée, comme indiquée sur l'événement :

* latitude 46.2
* longitude -0.73

![station url builder, geographical selection](imgs/station-url-builder.png)

Rajouter une sélection de dates pour avoir les stations actives à ce moment là :

* start 2023-06-16
* end 2023-06-17

L'URL de la requête ressemblera à :

* pour une sortie au format StationXML : [https://ws.resif.fr/fdsnws/station/1/query?starttime=2023-06-16T00:00:00&includerestricted=true&endtime=2023-06-17T00:00:00&level=channel&format=xml&lon=-0.7&lat=46.1&maxradius=2&nodata=404](https://ws.resif.fr/fdsnws/station/1/query?starttime=2023-06-16T00:00:00&includerestricted=true&endtime=2023-06-17T00:00:00&level=channel&format=xml&lon=-0.7&lat=46.1&maxradius=2&nodata=404)

* pour une sortie au format texte : [https://ws.resif.fr/fdsnws/station/1/query?starttime=2023-06-16T00:00:00&includerestricted=true&endtime=2023-06-17T00:00:00&level=channel&format=txt&lon=-0.7&lat=46.1&maxradius=2&nodata=404](https://ws.resif.fr/fdsnws/station/1/query?starttime=2023-06-16T00:00:00&includerestricted=true&endtime=2023-06-17T00:00:00&level=channel&format=txt&lon=-0.7&lat=46.1&maxradius=2&nodata=404)
