# Vérifier la disponibilité des données

Nous allons réaliser une requête au webservice `fdsn-availability`. Pour cela, on peut s'appuyer sur l'URL builder de availability: [https://seismology.resif.fr/url-builder-availability-v-1/](https://seismology.resif.fr/url-builder-availability-v-1/#/)

Renseigner les paramètres de recherche à la manière de cette illustration :

![Availability URL builder](imgs/availability-url-builder.png)

Choisir l'un des deux modes de requêtes :

* query
* extent

L'URL résultante ressemblera à ceci: [https://ws.resif.fr/fdsnws/availability/1/query?station=ABJF,BEGF,BOUF,CHIF,BSCF,CRNF&starttime=2023-06-16T00:00:00&endtime=2023-10-16T23:59:00&format=text&merge=overlap&orderby=nslc_time_quality_samplerate&includerestricted=false&nodata=204](https://ws.resif.fr/fdsnws/availability/1/query?station=ABJF,BEGF,BOUF,CHIF,BSCF,CRNF&starttime=2023-06-16T00:00:00&endtime=2023-10-16T23:59:00&format=text&merge=overlap&orderby=nslc_time_quality_samplerate&includerestricted=false&nodata=204)


