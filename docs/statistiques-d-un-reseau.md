## Obtenir des statistiques pour un réseau

On peut consulter les statistiques de distribution de la donnée au niveau Européen, grâce au tableau de bord des statistiques.
https://orfeus-eu.org/data/eida/stats/ 

![Statistics dashboard](imgs/stats.png)

