# Récupérer les données

Après avoir exploré les métadonnées, nous pouvons télécharger les séries temporelles.

## Webservice dataselect

On peut construire une requête à l'aide de l'URL builder : [https://seismology.resif.fr/dataselect/#/](https://seismology.resif.fr/dataselect/#/)

La sélection par station permet de choisir une zone géographique.

Indiquer `quality = M`.

![Dataselect station selection](imgs/dataselect-selection.png)


L'URL résultante sera : [https://ws.resif.fr/fdsnws/dataselect/1/query?station=ABJF,CHIF,CLMF,GNEF,LGIF,MFF,MFT2,PSIS1,PSIS2,PSIS3,PSIS4,PSIS5,PY40,PY41,PY42A,PY42B,PY43,PY44,PY96,SDOF,SMFF,UNCO,UNIO&quality=B&starttime=2023-06-16T16:38:00&endtime=2023-06-16T16:41:00&nodata=204](https://ws.resif.fr/fdsnws/dataselect/1/query?station=ABJF,CHIF,CLMF,GNEF,LGIF,MFF,MFT2,PSIS1,PSIS2,PSIS3,PSIS4,PSIS5,PY40,PY41,PY42A,PY42B,PY43,PY44,PY96,SDOF,SMFF,UNCO,UNIO&quality=B&starttime=2023-06-16T16:38:00&endtime=2023-06-16T16:41:00&nodata=204)

Les données sont récupérées au format miniseed.

## Smart clients

Il est important de s'équiper de bons outils. Pour la récupération des données, on peut mentionner:

* fdsnws-scripts: https://fdsnwsscripts.readthedocs.io/en/latest/userdoc.html
* ObsPy: [https://docs.obspy.org/](https://docs.obspy.org/)



