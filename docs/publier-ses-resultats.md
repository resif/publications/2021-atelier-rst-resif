# Publier ses résultats

La citation des données est essentielle. Nous faisons en sorte que cela soit facile pour les chercheurs :

- les réseaux sismologiques disposent d'un DOI qui sont listés sur : [https://seismology.resif.fr/doi-for-networks/](https://seismology.resif.fr/doi-for-networks/)
- le centre de données dispose de son identifiant persistant également : http://doi.org/10.17616/R37Q06 

Il faut citer en référence bibliographique chaque réseau utilisé. 

- La /landing page/ de chaque réseau présente le texte de citation
- [https://search.datacite.org/works/DOI](https://search.datacite.org/works/DOI) permet de retrouver un texte de citation dans différents formats (y compris bibtex). Par exemple pour RA: [https://commons.datacite.org/doi.org/10.15778/resif.ra](https://commons.datacite.org/doi.org/10.15778/resif.ra)
- Le service de citations de la FDSN permet de composer des citations à partir d'un fichier de données : [http://fdsn.org/networks/citation/](http://fdsn.org/networks/citation/)

