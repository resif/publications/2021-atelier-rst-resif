## Utiliser les portails

### Portail de WebServices

[https://ws.resif.fr/](https://ws.resif.fr/)

### Portail des données sismologiques

[https://seismology.resif.fr/](https://seismology.resif.fr/)
