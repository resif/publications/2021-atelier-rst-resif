# Utiliser timeseries

On peut reprendre la même URL pour interroger `resifws-timeseries` et voir les [sismogrammes déconvolués de la réponse instrumentale](https://ws.resif.fr/resifws/timeseries/1/query?station=ABJF,CHIF,CLMF,GNEF,LGIF,MFF,MFT2,PSIS1,PSIS2,PSIS3,PSIS4,PSIS5,PY40,PY41,PY42A,PY42B,PY43,PY44,PY96,SDOF,SMFF,UNCO,UNIO&starttime=2023-06-16T16:38:00&endtime=2023-06-16T16:41:00&cha=??N&nodata=204&format=plot)

Timeseries dispose de nombreuses options qui peuvent être consultées sur [sa page de documentation](https://ws.resif.fr/resifws/timeseries/1/).

Le webservice [timeseriesplot](https://ws.resif.fr/resifws/timeseriesplot/1/) (assez proche de timeseries) propose des options pour la visualisation interactive des séries temporelles.
