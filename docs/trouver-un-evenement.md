# Trouver un événement

Nous allons travailler sur l'événement de magnitude 5.3 du 16 juin 2023 près de Niort.

Pour cela, nous commençons par rechercher dans la base de données des événements du RENASS: [https://api.franceseisme.fr/fr/search](https://api.franceseisme.fr/fr/search).

Utiliser différents critères pour filtrer au mieux les résultats :
- Choisir les dates de début et de fin (15/06/2023 → 17/06/2023)
- Indiquer une plage de magnitude (5 → 6)
- Indiquer une zone géographique ...


On récupère une liste d'événements, et on peut consulter celui qui nous intéresse par son lien direct: [https://renass.unistra.fr/fr/evenements/fr2023lznjuc/](https://renass.unistra.fr/fr/evenements/fr2023lznjuc/)

## Avec Obspy

Ce [notebook](https://nbviewer.org/github/fabienengels/notebooks/blob/main/obspy-fdsn-event/FDSN%20Event%20FranceSeisme.ipynb) montre un exemple interactif pour l'utilisation de Obspy avec le webservice events.

